# GitLab Ubuntu Docker Deploy

Deploy GitLab using Docker on Ubuntu

## Usage
`sudo ./deploy -d {DNS}`  
To use Docker Compose use  
`sudo ./deploy -cd {DNS}`
